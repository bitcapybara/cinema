#![allow(dead_code)]

mod api;
mod audience;
mod config;
mod error;
mod room;
mod types;
mod user;

use std::{net::SocketAddr, sync::Arc};

use audience::AudienceService;
use axum::{
    routing::{get, post},
    Extension, Router,
};

pub use error::{Error, Result};
pub use log::*;
use room::RoomService;
use user::UserService;

#[tokio::main]
async fn main() {
    env_logger::init();
    config::init_config("./config/application.yaml")
        .await
        .unwrap();

    let app = Router::new()
        .route("/login", post(api::login))
        .route("/ws/room/join", post(api::join_room))
        .route("/room/create", post(api::create_room))
        .route("/room/list", get(api::list_room))
        .route("/audience/list", get(api::list_audience))
        .layer(Extension(Arc::new(RoomService::new())))
        .layer(Extension(Arc::new(AudienceService::new())))
        .layer(Extension(Arc::new(UserService::new())));

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    info!("server start at {}", &addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
