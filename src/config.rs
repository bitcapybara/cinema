use anyhow::Context;
use once_cell::sync::OnceCell;
use tokio::fs;
use tokio::io::AsyncReadExt;

use crate::Result;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Config {
    pub admin: Admin,
    pub token: Token,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Admin {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Token {
    pub secret: String,
}

pub static CONFIG: OnceCell<Config> = OnceCell::new();

pub async fn init_config(cfg: &str) -> Result<()> {
    let mut file = fs::File::open(cfg).await.context("open config file err")?;

    let mut contents = vec![];
    file.read_to_end(&mut contents)
        .await
        .context("read config file content err")?;
    let cfg: Config = serde_yaml::from_slice(&contents).context("parse config err")?;
    CONFIG.get_or_init(|| cfg);

    Ok(())
}

pub fn get_admin() -> Admin {
    CONFIG.get().unwrap().admin.clone()
}
