use crate::error::Result;

pub struct UserService {}

impl UserService {
    pub fn new() -> Self {
        Self {}
    }

    /// ### 从存储中获取用户信息
    ///
    /// Parameters
    ///
    /// 1. username 用户名
    ///
    /// Returns
    ///
    /// 1. 返回用户信息
    /// 2. 用户不存在，返回 ErrUserNotFound
    pub fn get_user(&self, username: &str) -> Result<User> {
        Ok(User {
            username: username.into(),
            password: "123456".into(),
        })
    }

    /// ### 验证用户名密码
    ///
    /// Parameters
    ///
    /// 1. username 用户名
    /// 2. password 密码
    pub async fn authenticate(&self, _username: &str, _password: &str) -> Result<()> {
        Ok(())
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct User {
    username: String,
    password: String,
}
