use std::fmt::Display;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("server error {0:?}")]
    Server(ServerError),

    #[error("{0:?}")]
    Auth(AuthError),

    #[error("{0:?}")]
    Unknown(#[from] anyhow::Error),
}

impl Error {
    /// ### 入参为空
    pub fn empty_parameter(msg: &str) -> Self {
        Self::Server(ServerError {
            code: 1001,
            message: format!("ErrEmptyParameter: {}", msg),
        })
    }

    /// ### 用户未找到
    pub fn user_not_found(msg: &str) -> Self {
        Self::Server(ServerError {
            code: 1002,
            message: format!("ErrUserNotFound: {}", msg),
        })
    }
    pub fn miss_token() -> Self {
        Self::Server(ServerError {
            code: 1003,
            message: "ErrAuthMissToken".into(),
        })
    }

    pub fn not_admin() -> Self {
        Self::Auth(AuthError::NotAdmin)
    }

    pub fn invalid_parameter(msg: &str) -> Self {
        Self::Server(ServerError {
            code: 1004,
            message: format!("ErrInvalidParameter: {}", msg),
        })
    }
}

#[derive(Debug)]
pub struct ServerError {
    code: usize,
    message: String,
}

impl ServerError {
    pub fn code(&self) -> usize {
        self.code
    }

    pub fn message(&self) -> String {
        self.message.clone()
    }
}

#[derive(Debug)]
pub enum AuthError {
    NotAdmin,
}

impl Display for AuthError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AuthError::NotAdmin => write!(f, "need admin authorization"),
        }
    }
}
