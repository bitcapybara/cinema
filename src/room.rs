use std::{
    cmp::Reverse,
    collections::HashMap,
    sync::{
        atomic::{self, AtomicUsize},
        Arc,
    },
};

use rand::{distributions::Alphanumeric, Rng};
use tokio::sync::{broadcast, Mutex};

use crate::{
    types::{List, Page},
    Result,
};

pub struct RoomService {
    // 当前所有房间，key 为 room_name
    rooms: Arc<Mutex<HashMap<String, Arc<Room>>>>,
}

impl RoomService {
    pub fn new() -> Self {
        Self {
            rooms: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub async fn new_room(&self, params: &CreateRoomParams<'_>) -> Result<Arc<Room>> {
        let room = Arc::new(Room::new(params)?);

        let mut rooms = self.rooms.lock().await;

        // 是否已存在
        if rooms.contains_key(params.room_name) {
            return Ok(rooms.get(params.room_name).unwrap().clone());
        }
        // 添加
        rooms.insert(params.room_name.into(), room.clone());

        Ok(room)
    }

    pub async fn get_room(&self, room_name: &str) -> Result<Option<Arc<Room>>> {
        Ok(self.rooms.lock().await.get(room_name).cloned())
    }

    pub async fn list_room(&self, page: Page) -> Result<List<Arc<Room>>> {
        let rooms = self.rooms.lock().await;
        let mut room_values = rooms.values().cloned().collect::<Vec<Arc<Room>>>();

        // 排序
        room_values.sort_by_key(|v| Reverse(v.create_at));
        // 分页
        let data = room_values
            .iter()
            .skip(page.from)
            .take(page.limit)
            .cloned()
            .collect::<Vec<Arc<Room>>>();

        Ok(List {
            total: room_values.len(),
            data,
        })
    }
}

pub struct Room {
    // 放映室名称
    pub room_name: String,
    // 影片名称
    pub video_name: String,
    // 影片地址
    pub video_url: String,
    // 创建人
    pub creator: String,
    // 观看人数
    audience_num: AtomicUsize,
    // 创建时间
    pub create_at: i64,
    // 邀请码
    pub ticket: String,

    // 每个房间，持有一个广播 tx
    bc_tx: broadcast::Sender<()>,
}

pub struct CreateRoomParams<'a> {
    // 放映室名称
    pub room_name: &'a str,
    // 影片地址
    pub video_url: &'a str,
    // 创建人
    pub creator: &'a str,
    // 影片名称
    pub video_name: &'a str,
}

impl Room {
    fn new(params: &CreateRoomParams) -> Result<Self> {
        // 生成邀请码
        let ticket: String = rand::thread_rng()
            .sample_iter(Alphanumeric)
            .take(6)
            .map(char::from)
            .collect();
        Ok(Self {
            room_name: params.room_name.into(),
            video_url: params.video_url.into(),
            creator: params.creator.into(),
            bc_tx: broadcast::channel(100).0,
            video_name: params.video_name.into(),
            audience_num: AtomicUsize::new(0),
            create_at: time::OffsetDateTime::now_utc().unix_timestamp(),
            ticket,
        })
    }

    pub fn subscribe(&self) -> broadcast::Receiver<()> {
        self.bc_tx.subscribe()
    }

    pub fn send(&self) -> Result<()> {
        // 发送给 bc_tx
        todo!()
    }

    pub fn audience_inc(&self) {
        self.audience_num.fetch_add(1, atomic::Ordering::SeqCst);
    }

    pub fn audience_dec(&self) {
        self.audience_num.fetch_sub(1, atomic::Ordering::SeqCst);
    }

    pub fn audience_num(&self) -> usize {
        self.audience_num.load(atomic::Ordering::SeqCst)
    }
}
