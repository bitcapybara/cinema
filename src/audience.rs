use std::{
    cmp::Reverse,
    collections::HashMap,
    sync::{
        atomic::{self, AtomicUsize},
        Arc,
    },
};

use tokio::sync::{broadcast, Mutex};

use crate::{
    room::Room,
    types::{List, Page},
    Error, Result,
};

pub struct AudienceService {
    // 当前所有的观众连接，key 为 username
    audiences: Arc<Mutex<HashMap<String, Arc<Audience>>>>,
}

impl AudienceService {
    pub fn new() -> Self {
        Self {
            audiences: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub async fn list_audience(&self, room_name: &str, page: Page) -> Result<List<Arc<Audience>>> {
        let audiences = self.audiences.lock().await;
        // 筛选
        let mut filtered = audiences
            .values()
            .filter(|v| v.room.room_name == room_name)
            .cloned()
            .collect::<Vec<Arc<Audience>>>();
        // 排序
        filtered.sort_by_key(|v| Reverse(v.join_at));
        // 分页
        let data = filtered
            .iter()
            .skip(page.from)
            .take(page.limit)
            .cloned()
            .collect::<Vec<Arc<Audience>>>();

        Ok(List {
            total: filtered.len(),
            data,
        })
    }

    pub async fn new_audience(
        &self,
        params: &NewAudienceParams<'_>,
        room: Arc<Room>,
    ) -> Result<Arc<Audience>> {
        let audience = Arc::new(Audience::new(params, room.clone()));
        let mut audiences = self.audiences.lock().await;
        if audiences.contains_key(params.username) {
            return Err(Error::invalid_parameter(&format!(
                "audience with username {} already exists",
                params.username
            )));
        }
        // 添加
        audiences.insert(params.username.into(), audience.clone());
        // 房间内人数+1
        room.audience_inc();
        Ok(audience)
    }

    pub async fn remove_audience(&self, username: &str) -> Result<()> {
        let mut audiences = self.audiences.lock().await;
        if let Some(audience) = audiences.get(username) {
            // 房间内人数-1
            audience.room.audience_dec();
        }
        // 移除
        audiences.remove(username);
        Ok(())
    }
}

pub struct NewAudienceParams<'a> {
    pub nickname: &'a str,
    pub username: &'a str,
    pub room_name: &'a str,
    pub ticket: &'a str,
}

pub struct Audience {
    // 昵称
    pub nickname: String,
    // 用户名
    pub username: String,
    // 播放进度
    progress: AtomicUsize,
    // 加入放映室时间
    pub join_at: i64,
    // 每个观众，只能在一个房间里
    room: Arc<Room>,
    // 接收房间广播的数据
    bc_rx: broadcast::Receiver<()>,
}

impl Audience {
    pub fn new(params: &NewAudienceParams<'_>, room: Arc<Room>) -> Self {
        Self {
            nickname: params.nickname.into(),
            username: params.username.into(),
            progress: AtomicUsize::new(0),
            join_at: time::OffsetDateTime::now_utc().unix_timestamp(),
            room: room.clone(),
            bc_rx: room.subscribe(),
        }
    }

    pub async fn broadcast(&self) -> Result<()> {
        // room.send()
        todo!()
    }

    pub async fn recv(&self) -> Result<()> {
        // bc_rx.recv()
        todo!()
    }

    pub fn progress(&self) -> usize {
        self.progress.load(atomic::Ordering::SeqCst)
    }
}
