use std::sync::Arc;

use serde_with::rust::display_fromstr::deserialize as deserialize_fromstr;

use crate::{
    audience::Audience,
    room::Room,
    types::{List, Page},
    Error, Result,
};

pub trait ParameterValidator {
    fn validate(&self) -> Result<()>;
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct LoginRequest {
    pub username: String, // 用户名
    pub password: String, // 加密后的密码
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct LoginResponse {
    pub access_token: String, // 用户凭证
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct JoinRoomRequest {
    pub nickname: String,  // 用户昵称（房间内使用）
    pub room_name: String, // 要加入的房间名称
    pub ticket: String,    // 房间邀请码
    pub token: String,     // 用户凭证
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct RoomDetail {
    room: RoomData,         // 房间信息
    audience: AudienceData, // 观众用户信息
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct RoomData {
    pub room_name: String,   // 放映室名称
    pub video_name: String,  // 影片名称
    pub video_url: String,   // 影片地址
    pub audience_num: usize, // 观看人数
    pub create_at: i64,      // 放映室创建时间
    pub creator: String,     // 创建人
    pub ticket_code: String, // 房间邀请码（仅房主可见）
}

impl From<Arc<Room>> for RoomData {
    fn from(room: Arc<Room>) -> Self {
        Self {
            room_name: room.room_name.clone(),
            video_name: room.video_name.clone(),
            video_url: room.video_url.clone(),
            audience_num: room.audience_num(),
            create_at: room.create_at,
            creator: room.creator.clone(),
            ticket_code: room.ticket.clone(),
        }
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct AudienceData {
    nickname: String, // 用户昵称（房间内使用）
    username: String, // 用户名称（仅房主可见）
    progress: usize,  // 影片播放进度
    join_at: i64,     // 入场时间
}

impl From<Arc<Audience>> for AudienceData {
    fn from(audience: Arc<Audience>) -> Self {
        Self {
            nickname: audience.nickname.clone(),
            username: audience.username.clone(),
            progress: audience.progress(),
            join_at: audience.join_at,
        }
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CreateRoomRequest {
    pub room_name: String,             // 放映室名称
    pub video_name: String,            // 影片名称
    pub video_url: String,             // 影片地址
    pub audience_limit: Option<usize>, // 房间内人数限制（默认 50 ）
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct ListRoomRequest {
    #[serde(flatten)]
    pub page: PageRequest,
}

impl ParameterValidator for ListRoomRequest {
    fn validate(&self) -> Result<()> {
        self.page.validate()
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct ListAudienceRequest {
    #[serde(flatten)]
    pub page: PageRequest,
    pub room_name: String, // 房间名称
}

impl ParameterValidator for ListAudienceRequest {
    fn validate(&self) -> Result<()> {
        self.page.validate()?;

        if self.room_name.is_empty() {
            return Err(Error::invalid_parameter("room name is empty"));
        }

        Ok(())
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct ListResponse<T> {
    pub page: PageResponse, // 分页信息
    pub data: Vec<T>,       // 数据列表
}

#[derive(Debug, Clone, Copy, serde::Serialize, serde::Deserialize)]
pub struct PageRequest {
    #[serde(deserialize_with = "deserialize_fromstr")]
    pub page_num: usize, // 当前页
    #[serde(deserialize_with = "deserialize_fromstr")]
    pub page_size: usize, // 页大小
}

impl ParameterValidator for PageRequest {
    fn validate(&self) -> Result<()> {
        if self.page_size == 0 || self.page_size > 100 {
            return Err(Error::invalid_parameter("page size must in [1,100]"));
        }
        if self.page_num == 0 {
            return Err(Error::invalid_parameter("page num is zero"));
        }
        Ok(())
    }
}

impl From<PageRequest> for Page {
    fn from(page: PageRequest) -> Self {
        Self {
            from: page.page_size * (page.page_num - 1),
            limit: page.page_size,
        }
    }
}

impl<A, B: From<A>> From<(PageRequest, List<A>)> for ListResponse<B> {
    fn from(list: (PageRequest, List<A>)) -> Self {
        let data = list
            .1
            .data
            .into_iter()
            .map(|v| v.into())
            .collect::<Vec<B>>();
        Self {
            page: PageResponse {
                total: list.1.total,
                page_num: list.0.page_num,
                page_size: list.0.page_size,
            },
            data,
        }
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct PageResponse {
    pub total: usize,     // 数据总量
    pub page_num: usize,  // 当前页
    pub page_size: usize, // 页大小
}
