use std::sync::Arc;

use axum::extract::ws::{Message, WebSocket};
use futures::{SinkExt, StreamExt};
use log::warn;

use crate::{
    audience::{AudienceService, NewAudienceParams},
    room::RoomService,
    Error, Result,
};

use super::{auth, types::JoinRoomRequest};

pub(super) async fn handle_socket(
    ws: WebSocket,
    req: JoinRoomRequest,
    room_service: Arc<RoomService>,
    audience_service: Arc<AudienceService>,
) -> Result<()> {
    // 获取用户名
    let username = auth::parse_token(&req.token)
        .map(|v| v.username)
        .unwrap_or_default();
    if username.is_empty() {
        return Err(Error::invalid_parameter("invalid token"));
    }
    // 获取房间
    let room = room_service
        .get_room(&req.room_name)
        .await?
        .ok_or_else(|| Error::invalid_parameter("room not found"))?;
    // 新建 audience
    let audience = audience_service
        .new_audience(
            &NewAudienceParams {
                nickname: &req.nickname,
                username: &username,
                room_name: &req.room_name,
                ticket: &req.ticket,
            },
            room.clone(),
        )
        .await?;

    let (mut sender, mut receiver) = ws.split();

    // 接收 ws 的数据，处理，发送到 audience
    let audience_s = audience.clone();
    let mut recv_task = tokio::spawn(async move {
        while let Some(Ok(Message::Text(_text))) = receiver.next().await {
            if let Err(e) = audience_s.broadcast().await {
                warn!("audience broatcast msg err: {:#}", e)
            }
        }
    });

    // 接收 audience 的数据，处理，发送到 ws
    let audience_r = audience.clone();
    let mut send_task = tokio::spawn(async move {
        while let Ok(_msg) = audience_r.recv().await {
            if let Err(e) = sender.send(Message::Text("".into())).await {
                warn!("send websocket msg err: {}", e)
            }
        }
    });

    // 关闭协程
    tokio::select! {
        _ = (&mut send_task) => recv_task.abort(),
        _ = (&mut recv_task) => send_task.abort(),
    };

    // 资源清理
    audience_service.remove_audience(&username).await?;

    Ok(())
}

pub enum Command {
    // 影片控制
    Control(Control),
    // 聊天信息
    ChatMessage(String),
}

pub enum Control {
    Play,            // 播放
    Pause,           // 暂停
    Progress(usize), // 进度条
}
