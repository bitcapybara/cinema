use std::{ops::Add, time::Duration};

use anyhow::Context;
use axum::{
    async_trait,
    extract::{rejection::TypedHeaderRejectionReason, FromRequest, RequestParts, TypedHeader},
};
use headers::{authorization::Bearer, Authorization};
use jsonwebtoken as jwt;
use log::debug;
use once_cell::sync::Lazy;

use crate::{config::CONFIG, Error};

static KEYS: Lazy<Keys> = Lazy::new(|| {
    let token = CONFIG.get().expect("config is empty").token.clone();
    Keys::new(token.secret.as_bytes())
});

const TOKEN_EXPIRE_DURATION: Duration = Duration::from_secs(6 * 60 * 60);

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Claims {
    pub username: String,
    pub exp: i64,
}

#[async_trait]
impl<B> FromRequest<B> for Claims
where
    B: Send,
{
    type Rejection = Error;

    async fn from_request(req: &mut RequestParts<B>) -> Result<Self, Self::Rejection> {
        // Extract the token from the authorization header
        let bearer_header = TypedHeader::<Authorization<Bearer>>::from_request(req).await;
        match bearer_header {
            Ok(TypedHeader(Authorization(bearer))) => Ok(parse_token(bearer.token())?),
            Err(e) => match e.reason() {
                TypedHeaderRejectionReason::Missing => Err(Error::miss_token()),
                _ => Err(e).context("get bearer header err")?,
            },
        }
    }
}

struct Keys {
    encoding: jwt::EncodingKey,
    decoding: jwt::DecodingKey,
}

impl Keys {
    fn new(secret: &[u8]) -> Self {
        Self {
            encoding: jwt::EncodingKey::from_secret(secret),
            decoding: jwt::DecodingKey::from_secret(secret),
        }
    }
}
pub(super) fn gen_token(username: &str) -> crate::Result<String> {
    let exp = time::OffsetDateTime::now_utc()
        .add(TOKEN_EXPIRE_DURATION)
        .unix_timestamp();
    debug!("{}", exp);
    Ok(jwt::encode(
        &jwt::Header::default(),
        &Claims {
            username: username.into(),
            exp,
        },
        &KEYS.encoding,
    )
    .context("encode token err")?)
}

pub(super) fn parse_token(token: &str) -> crate::Result<Claims> {
    Ok(
        jwt::decode::<Claims>(token, &KEYS.decoding, &jwt::Validation::default())
            .context("decode token err")?
            .claims,
    )
}
