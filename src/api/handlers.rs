use std::sync::Arc;

use axum::{
    extract::{ws::WebSocket, Query, WebSocketUpgrade},
    response::Response,
    Extension, Json,
};
use log::warn;

use crate::{
    audience::AudienceService,
    config,
    room::{CreateRoomParams, RoomService},
    user::UserService,
    Error,
};

use super::{
    auth,
    response::{CommonResponse, Result},
    types::{
        AudienceData, CreateRoomRequest, JoinRoomRequest, ListAudienceRequest, ListResponse,
        LoginResponse, RoomData,
    },
    types::{ListRoomRequest, LoginRequest, ParameterValidator},
    ws_handler,
};

/// ### 用户登录
///
/// Parameters
///
/// 1. payload 登录参数
///
/// Returns
///
/// 1. 登录信息，包括新的 token
pub async fn login(
    Json(payload): Json<LoginRequest>,
    Extension(user_service): Extension<Arc<UserService>>,
) -> Result<LoginResponse> {
    let username = &payload.username;
    let password = &payload.password;

    // 入参校验
    if username.is_empty() || password.is_empty() {
        return Err(Error::empty_parameter("username or password is empty"));
    }

    // 验证用户名密码
    user_service.authenticate(username, password).await?;

    // 生成新 token
    let token = auth::gen_token(username)?;

    Ok(CommonResponse::ok(LoginResponse {
        access_token: token,
    }))
}

/// ### 创建房间，实质上是建立一个 ws 连接
///
/// Parameters
///
/// 1. token 用户凭证，仅注册用户可以创建房间
/// 2. payload 创建房间参数
///
/// Returns
///
/// 1. 放映室房间详情，用于前端展示放映室页面
pub async fn create_room(
    token: auth::Claims,
    Json(payload): Json<CreateRoomRequest>,
    Extension(room_service): Extension<Arc<RoomService>>,
) -> Result<RoomData> {
    let username = token.username;

    // 创建出新房间
    let room = room_service
        .new_room(&CreateRoomParams {
            room_name: &payload.room_name,
            video_url: &payload.video_url,
            creator: &username,
            video_name: &payload.video_name,
        })
        .await?;

    Ok(CommonResponse::ok(room.into()))
}

/// ### 加入放映室
///
/// Parameters
///
/// 1. token 用户凭证，为空代表游客
/// 2. payload 加入放映室参数
///
/// Returns
///
/// 1. 放映室房间详情，用于前端展示放映室页面
pub async fn join_room(
    Query(query): Query<JoinRoomRequest>,
    ws: WebSocketUpgrade,
    Extension(room_service): Extension<Arc<RoomService>>,
    Extension(audience_service): Extension<Arc<AudienceService>>,
) -> Response {
    ws.on_upgrade(|ws| ws_handler_wrapper(ws, query, room_service, audience_service))
}

async fn ws_handler_wrapper(
    ws: WebSocket,
    req: JoinRoomRequest,
    room_service: Arc<RoomService>,
    audience_service: Arc<AudienceService>,
) {
    if let Err(e) = ws_handler::handle_socket(ws, req, room_service, audience_service).await {
        warn!("websocket handle err: {:#}", &e)
    }
}

/// ### 列出当前放映室
///
/// Parameters
///
/// 1. token 管理员凭证，仅管理员可以查询放映室列表
/// 2. payload 查询参数，包括分页参数
///
/// Returns
///
/// 1. 分页后的放映室列表
pub async fn list_room(
    token: auth::Claims,
    Query(query): Query<ListRoomRequest>,
    Extension(room_service): Extension<Arc<RoomService>>,
) -> Result<ListResponse<RoomData>> {
    // 参数校验
    query.validate()?;

    // 必须是管理员
    if token.username != config::get_admin().username {
        return Err(Error::not_admin());
    }

    // 按时间排序取出数据
    let rooms = room_service.list_room(query.page.into()).await?;

    Ok(CommonResponse::ok((query.page, rooms).into()))
}

/// ### 列出放映室下的观众信息
///
/// Parameters
///
/// 1. token 用户凭证，仅注册用户可以查询观众列表
/// 2. payload 查询参数
///
/// Returns
///
/// 1. 分页后的观众列表
pub async fn list_audience(
    _token: auth::Claims,
    Query(query): Query<ListAudienceRequest>,
    Extension(audience_service): Extension<Arc<AudienceService>>,
) -> Result<ListResponse<AudienceData>> {
    // 参数校验
    query.validate()?;

    // 搜索
    let audiences = audience_service
        .list_audience(&query.room_name, query.page.into())
        .await?;

    Ok(CommonResponse::ok((query.page, audiences).into()))
}
