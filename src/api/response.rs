use std::fmt::Debug;

use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};

use crate::{error::ServerError, Error};

pub type Result<T> = crate::Result<CommonResponse<T>>;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommonResponse<T: serde::Serialize> {
    code: usize,     // 非 0 代表失败
    message: String, // 错误信息
    data: Option<T>, // 返回的数据
}

impl<T: serde::Serialize> CommonResponse<T> {
    pub fn ok(data: T) -> Self {
        Self {
            code: 0,
            message: "ok".into(),
            data: Some(data),
        }
    }

    pub fn err(err: ServerError) -> Self {
        Self {
            code: err.code(),
            message: err.message(),
            data: None,
        }
    }
}

impl<T: serde::Serialize> IntoResponse for CommonResponse<T> {
    fn into_response(self) -> Response {
        (StatusCode::OK, Json(self)).into_response()
    }
}

impl IntoResponse for crate::error::Error {
    fn into_response(self) -> Response {
        // ServerError 返回 CommonResponse
        // 其他，返回 500 错误
        match self {
            Error::Server(e) => CommonResponse::<()>::err(e).into_response(),
            Error::Unknown(e) => {
                (StatusCode::INTERNAL_SERVER_ERROR, format!("{:#}", e)).into_response()
            }
            Error::Auth(e) => (StatusCode::FORBIDDEN, format!("{}", e)).into_response(),
        }
    }
}
