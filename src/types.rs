pub struct List<T> {
    pub total: usize,
    pub data: Vec<T>,
}

pub struct Page {
    pub from: usize,
    pub limit: usize,
}
